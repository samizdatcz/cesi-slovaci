---
title: "Češi/Slováci - grafy pro Wave"
perex: ""
description: ""
authors: ["Michal Zlatkovský"]
published: "3. dubna 2017"
libraries: [jquery, highcharts]
---

<aside class="big">
<iframe src="https://interaktivni.rozhlas.cz/data/cesi-slovaci/www/politika1.html" width="100%" height="600" scrolling="no" frameborder="0"></iframe>
</aside>

<aside class="big">
<iframe src="https://interaktivni.rozhlas.cz/data/cesi-slovaci/www/politika2.html" width="100%" height="600" scrolling="no" frameborder="0"></iframe>
</aside>

<aside class="big">
<iframe src="https://interaktivni.rozhlas.cz/data/cesi-slovaci/www/zivot1.html" width="100%" height="600" scrolling="no" frameborder="0"></iframe>
</aside>

<aside class="big">
<iframe src="https://interaktivni.rozhlas.cz/data/cesi-slovaci/www/zivot2.html" width="100%" height="600" scrolling="no" frameborder="0"></iframe>
</aside>

<aside class="big">
<iframe src="https://interaktivni.rozhlas.cz/data/cesi-slovaci/www/drogy.html" width="100%" height="600" scrolling="no" frameborder="0"></iframe>
</aside>
